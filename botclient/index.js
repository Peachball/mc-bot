import mineflayer from "mineflayer";
import Vec3 from "vec3";
import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import { readFile } from "fs/promises";
import minecraftData from 'minecraft-data';
import { Heap } from 'heap-js';
import HashMap from 'hashmap';
import { Movements, pathfinder, default as mineflayerPathfinder } from 'mineflayer-pathfinder';
import * as autoeat from 'mineflayer-auto-eat';


// Coefficients are retrieved empirically from measuring jump heights. They
// are set a little lower because this function is anticipated to be used to
// estimate if a jump can be made/from where. In general, we want to make sure
// we don't undershoot
const GRAVITY_COEFF1 = -22;
const GRAVITY_COEFF2 = 10;
const SPRINT_SPEED = 5.4;
const MAX_SAFE_BLOCK_DISTANCE = 3;
const mcData = minecraftData('1.21.1');

const argv = yargs(hideBin(process.argv)).option("config", {
  type: "string",
  describe: "Path to json config file",
  demandOption: true
}).argv;

const secrets = JSON.parse(
  await readFile(new URL(argv.config, import.meta.url))
);

function xyzArgs(yargs) {
  yargs.positional("x", {
    type: "number",
    demandOption: true
  });
  yargs.positional("y", {
    type: "number",
    demandOption: true
  });
  yargs.positional("z", {
    type: "number",
    demandOption: true
  });
}

function vecEqWithinBlock(v1, v2) {
  return v1.distanceTo(v2) < 1;
}

function initBot() {
  let bot = mineflayer.createBot(secrets);
  let botState = "receive-commands";

  function isTransparent(p) {
    return bot.blockAt(p).transparent;
  }

  async function startSyncCommand(command) {
    if (botState !== "receive-commands") {
      bot.chat("Performing another action, unable to execute");
      return;
    }
    botState = "executing-command";
    await command();
    botState = "receive-commands";
  }

  async function gotoPosition(destination, range=1) {
    let defaultMove = new Movements(bot);
    defaultMove.canDig = false;
    defaultMove.allowFreeMotion = true;
    bot.pathfinder.setMovements(defaultMove);
    bot.pathfinder.setGoal(null);
    await bot.pathfinder.goto(new mineflayerPathfinder.goals.GoalNear(destination.x, destination.y, destination.z, range))
      .catch(err => {
        bot.chat("Couldn't make it to the goal");
      });
  }

  function setDefaultMovement() {
    let defaultMove = new Movements(bot);
    defaultMove.canDig = false;
    defaultMove.allowFreeMotion = true;
    bot.pathfinder.setMovements(defaultMove);
  }

  function intArgs(yargs, names) {
    for (let name of names) {
      yargs.positional(
        name,
        {
          demandOption: true,
          type: "number"
        }
      );
    }
  }

  function findItem(items, itemName) {
    for (let item of items) {
      if (item.name === itemName) {
        return item;
      }
    }
    return null;
  }

  bot.loadPlugin(pathfinder);
  bot.loadPlugin(autoeat.loader);

  bot.once("spawn", () => {
    console.log("Bot has spawned");
  });

  bot.on("chat", (username, message) => {
    if (username === bot.username) {
      return;
    }
    if (!message.startsWith("?b ")) {
      return;
    }
    let strippedCommand = message.split("?b ", 2)[1];

    let parser = yargs()
      .command(
        "excavate <dx> <dy> <dz>",
        "Dig out a large space",
        yargs => {
          intArgs(yargs, ['dx', 'dy', 'dz']);
        },
        argv => {
          startSyncCommand(async () => {
            async function equipPick() {
              let stonePickItem = findItem(bot.inventory.items(), "stone_pickaxe");
              if (stonePickItem == null) {
                bot.chat("No stone pick available");
                return false;
              }
              await bot.equip(stonePickItem, "hand");
              return true;
            }
            const BLOCK_MATERIALS = [
              'mineable/pickaxe', 'mineable/shovel', 'incorrect_for_wooden_tool'
            ]
            let blocks = [];
            let ddx = argv.dx > 0 ? 1 : -1;
            let ddy = argv.dy > 0 ? 1 : -1;
            let ddz = argv.dz > 0 ? 1 : -1;
            let botPosition = bot.entity.position.floor();
            let concerningBlocks = [];
            for (let dx = 0; Math.abs(dx) < Math.abs(argv.dx); dx = dx + ddx) {
              for (let dz = 0; Math.abs(dz) < Math.abs(argv.dz); dz = dz + ddz) {
                for (let dy = 0; Math.abs(dy) < Math.abs(argv.dy); dy = dy + ddy) {
                  let difference = new Vec3(dx, dy, dz);
                  let blockTestPosition = difference.plus(botPosition);
                  let block = bot.blockAt(blockTestPosition);
                  if (block.transparent) {
                    continue;
                  }
                  if (BLOCK_MATERIALS.indexOf(block.material) === -1) {
                    concerningBlocks.push(block);
                    continue;
                  }
                  blocks.push(block);
                }
              }
            }
            if (concerningBlocks.length !== 0) {
              console.log(concerningBlocks);
              bot.chat(`Not sure how to deal with ${concerningBlocks.length} blocks, aborting`);
              return;
            }
            while (blocks.length > 0) {
              blocks.sort((a, b) => {
                return -(a.position.distanceTo(bot.entity.position) - b.position.distanceTo(bot.entity.position));
              });
              await gotoPosition(blocks[blocks.length - 1].position, 2);
              while (blocks.length > 0) {
                let closestBlock = blocks[blocks.length - 1];
                if (bot.getEquipmentDestSlot("hand") !== mcData.blocksByName['stone_pickaxe']) {
                  if (!await equipPick()) {
                    return;
                  }
                }
                if (!bot.canDigBlock(closestBlock)) {
                  break;
                }
                await bot.dig(closestBlock);
                blocks.pop();
              }
            }
            bot.chat('done');
          });
        }
      )
      .command(
        "blockAt <x> <y> <z>",
        "Debugging function to check where block is",
        xyzArgs,
        argv => {
          let p = new Vec3(argv.x, argv.y, argv.z);
          let block = bot.blockAt(p);
          if (block === null) {
            bot.chat("not sure what the block is");
            return;
          }
          bot.chat(`Block at ${p} is ${block.name}`);
        }
      )
      .command(
        "goto <x> <y> <z>",
        "Walk to a specific position",
        xyzArgs,
        argv => {
          startSyncCommand(async () => {
            // basic pathfinding that doesn't break blocks or do any advanced block jumping
            let destination = new Vec3(argv.x, argv.y, argv.z);
            setDefaultMovement();
            await gotoPosition(destination);
          });
        }
      )
      .command(
        "gotoPlayer <player>",
        "Walk to a specific player",
        yargs => {
          yargs.positional("player",
            {
              demandOption: true,
              type: "string"
            }
          )
        },
        argv => {
          startSyncCommand(async () => {
            // basic pathfinding that doesn't break blocks or do any advanced block jumping
            if (!bot.players[argv.player] || !bot.players[argv.player].entity || !bot.players[argv.player].entity.position) {
              bot.chat('Can\'t find player :(');
              return;
            }
            let destination = bot.players[argv.player].entity.position;
            setDefaultMovement();
            await gotoPosition(destination);
          });
        }
      )
      .command(
        "eat",
        "eat's food in inventory",
        yargs => { },
        argv => {
          startSyncCommand(async () => {
            bot.autoEat.eat();
          })
        }
      )
      .command(
        "follow <player>",
        "follows the player -- this is a utility function + doesn't fit with the rest of the commands",
        yargs => {
          yargs.positional("player",
            {
              demandOption: true,
              type: "string"
            }
          )
        },
        argv => {
          if (!bot.players[argv.player] || !bot.players[argv.player].entity || !bot.players[argv.player].entity.position) {
            bot.chat('Can\'t find player :(');
            return;
          }
          setDefaultMovement();
          bot.pathfinder.setGoal(null);
          bot.pathfinder.goto(new mineflayerPathfinder.goals.GoalFollow(bot.players[argv.player].entity, 1))
            .catch(err => {
              console.log(err);
              bot.chat("Couldn't make it to the goal");
            });
        }
      )
      .command(
        "jumpTo <x> <y> <z>",
        "Jump forward to a specified position",
        xyzArgs,
        argv => {
          let xPositions = [];
          let yPositions = [];
          let zPositions = [];
          const intervalId = setInterval(
            () => {
              xPositions.push(bot.entity.position.x);
              yPositions.push(bot.entity.position.y);
              zPositions.push(bot.entity.position.z);
              if (yPositions.length > 20) {
                bot.setControlState("jump", false);
                console.log(xPositions);
                console.log(yPositions);
                console.log(zPositions);
                clearInterval(intervalId);
              }
            },
            25
          );
          bot.setControlState("sprint", true);
          bot.setControlState("jump", true);
          bot.setControlState("forward", true);
        }
      )
      .command(
        "stats",
        "Information about stats",
        yargs => { },
        argv => {
          bot.chat(
            `Bot health: ${bot.health / 2}/10, hunger: ${bot.food / 2}/10, location: ${bot.entity.position}`
          );
        }
      )
      .command(
        'inventory',
        'Information about current bot inventory',
        yargs => { },
        argv => {
          let aggregatedCounts = new Map();
          for (let item of bot.inventory.items()) {
            const prevValue = aggregatedCounts.get(item.name);
            if (prevValue) {
              aggregatedCounts.set(item.name, item.count + prevValue);
            } else {
              aggregatedCounts.set(item.name, item.count);
            }
          }
          const itemCounts = aggregatedCounts.entries().map((kv) => {
            return `${kv[0]}, ${kv[1]}`
          })
            .toArray()
            .join(" ");
          bot.chat(`Item counts: ${itemCounts}`);
        }
      )
      .command(
        'drop <item>',
        'Drop all of the item',
        yargs => {
          yargs.positional(
            'item',
            {
              demandOption: true,
              type: "string"
            }
          )
        },
        argv => {
          startSyncCommand(async () => {
            for (let item of bot.inventory.items()) {
              if (item.name === argv.item) { }
              bot.tossStack(item);
            }
          });
        }
      )
      .command(
        'face <direction>',
        'Face a particular direction',
        yargs => {
          yargs.positional("direction", {
            type: "string",
            demandOption: true,
            choices: ['n', 's', 'e', 'w']
          });
        },
        argv => {
          const DIRECTION = {
            'e': 0,
            's': 1,
            'w': 2,
            'n': 3,
          };
          bot.look(DIRECTION[argv.direction] * Math.PI * 2 / 4, 0, true);
        }
      )
      .command(
        "logoff [time]",
        "Log off",
        yargs => {
          yargs.positional("time", {
            type: "number",
            description: "Number of seconds to log off for"
          });
        },
        argv => {
          startSyncCommand(() => {
            bot.quit();
            if (argv.time) {
              setTimeout(() => {
                initBot();
              }, argv.time);
            }
          });
        }
      )
      .demandCommand()
      .strict()
      .help()
      .showHelpOnFail(true);
    parser.parse(strippedCommand, (err, argv, output) => {
      console.log(err, argv, output);
      if (err) {
        bot.chat(`Error in parsing command: ${strippedCommand}`);
      }
    });
  });

  bot.on("error", err => {
    console.log("Ran into error:");
    console.log(err);
  });

  bot.on("kicked", err => {
    console.log("was kicked");
    console.log(err);
  });

  bot.on('rain', reportRain(bot));

  bot.on('autoeat_started', (item, offhand) => {
    console.log(`Eating ${item.name} in ${offhand ? 'offhand' : 'hand'}`)
  })

  bot.on('autoeat_finished', (item, offhand) => {
    console.log(`Finished eating ${item.name} in ${offhand ? 'offhand' : 'hand'}`)
  })

  bot.on('autoeat_error', console.error)
}

function reportRain(bot) {
  return () => {
    if (bot.isRaining) bot.chat('it is raining');
  }
}

initBot();
