# mc-bot

A script to play minecraft in an automated way.

## Usage

First install the required dependencies in the `botclient/` directory with the
command:
```
npm install
```

Then, create a file with server and account information in the `data/`
directory. Example:
```json
{
  "host": "xx.xx.xx.xx", // ipv4 address of server
  "port": 25565, // can omit this option if using default of 25565
  "username": "username",
  "password": "password",
  "auth": "microsoft"
}
```

Supposing the json file was created in `data/test.json`, you can run the bot
with the following command in the `botclient/` directory
```bash
npm run start -- --config ../data/test.json
```

Note that if you're using Powershell, you need to quote the "--" part because of
the different parsing scheme. 
```bash
npm run start "--" --config ../data/test.json
```

## Goals

- pathfinding
  - couple of options:
    - whether or not to break blocks
    - what types of blocks to break
- resource retrieval
  - ores
  - blocks
- expand farms
- survival
  - manage food
    - find and kill mobs
    - manage farms
  - kill mobs when in dangerous areas
  - avoid dying
- creating structures
  - given a picture, build an approximation of the picture using blocks
- estimated time to creation + scheduling
  - Update chat on progress of tasks
  - Given a set of tasks, find the optimal route to do all of them

## Todo

### Working on

- [chen] iron ore retrieval

### Backlog

### Done

## Brainstorming

### General system for solving tasks

Minecraft state is made up of a couple of attributes
- position
- relevant modifications to world state
  - this only impacts position/pathing
- inventory

example task for iron ore
- goal: inventory iron ore
  - iron ore position
  - inventory: stone pick
- goal: inventory stone pick
  - one of:
    - crafting table in inventory
    - crafting table position
  - inventory: stone + stick
- goal: inventory stone
  - inventory: wooden pick
  - position: stone
- goal: stick

etc.

group world into higher level positions, as items tend to appear in groups
- set of positions with attributes
  - resource type. e.g. tree, iron ore, stone

use approximation of shortest path
  - calculate distance from point to center of each other point, ignoring mining costs