import matplotlib.pyplot as plt
from scipy import odr, stats
import numpy as np

heights = [
    129,
    129.41999998688698,
    129.41999998688698,
    129.7531999805212,
    129.7531999805212,
    130.00133597911216,
    130.00133597911216,
    130.24918707874468,
    130.24918707874468,
    130.25220334025371,
    130.25220334025371,
    130.17675927506423,
    130.17675927506423,
    130.02442408821366,
    129.79673560066868,
    129.79673560066868,
    129.4952008770059,
    129.4952008770059,
    129,
]
distances = [
                 -4.5,  -4.172599949907642,
   -4.172599949907642,  -3.968359522557215,
   -3.968359522557215,  -3.539222435779437,
   -3.315545984700548,  -3.315545984700548,
   -3.086520414218759,  -3.086520414218759,
   -2.852627145080331,  -2.852627145080331,
  -2.6143042701643613, -2.6143042701643613,
  -2.1259284812729144, -2.1259284812729144,
  -1.8765684860996121, -1.8765684860996121,
  -1.6241708904919074
]

interval = 0.025

x = np.linspace(0, len(heights) * interval, len(heights))
data = odr.Data(x, heights)
odr_obj = odr.ODR(data, odr.quadratic)
output = odr_obj.run()
print(output.beta)
output.beta = [-22, 10, 129]
estimated = x * x * output.beta[0] + x * output.beta[1] + output.beta[2]

plt.subplot(121)
plt.plot(estimated)
plt.plot(heights)


print(x.shape, np.array(distances).shape)
slope, intercept, _, _, _ = stats.linregress(x, np.array(distances))
slope = 5.4

plt.subplot(122)
print(slope, intercept)
plt.plot(distances)
plt.plot(x*slope + intercept)
plt.show()
